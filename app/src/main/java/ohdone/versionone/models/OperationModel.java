package ohdone.versionone.models;

/**
 * Created by mohamedatef on 6/23/18.
 */

public class OperationModel {
    private int id,icon;
    private String title;
    private boolean isDeoposite;


    public OperationModel(int id , int icon , String title,boolean isDeoposite){
        this.id = id ;
        this.icon = icon;
        this.title = title;
        this.isDeoposite=isDeoposite;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public int getIcon() {
        return icon;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDeoposite() {
        return isDeoposite;
    }

    public void setDeoposite(boolean deoposite) {
        isDeoposite = deoposite;
    }
}
