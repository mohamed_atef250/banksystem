package ohdone.versionone.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import ohdone.versionone.R;


public class MUT {

    public static final String appColor = "#020224";

    //here you can find function to must used tools in the application like start activity , make call , etc.....

    public static void settingsDialog(final Activity activity, final String action, String title, String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message)
                .setPositiveButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {

                            }
                        })
                .setNegativeButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                                //  alertDialog.cancel();
                            }
                        });
        builder.create().show();
    }

    //<uses-permission android:name="android.permission.CALL_PHONE" />
    public static void makeCall(Context context, String number) {
        try {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
            context.startActivity(intent);
        }catch (Exception e){
            e.getStackTrace();
        }
    }


    public static void fastDialog(final Activity activity, String title, String message) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int id) {
            }
        });
        builder.create().show();
    }


    public static void lToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void sToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }


    public static void dToast(Context context, String text, int duration) {

        Toast.makeText(context, text, duration).show();

    }

    public static void startActivity(Context context, Class<?> activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);

    }

    public static void startWebPage(Context context, String page) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(page)));
        }
        catch (Exception e){
            e.getStackTrace();
        }
    }

    public static void shareContent(Context context , String shareSubject , String shareBody){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        final String appPackageName = context.getPackageName();
        shareBody += "\nhttps://play.google.com/store/apps/details?id=" + appPackageName;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSubject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent , "Share via"));
    }

    @SuppressLint("NewApi")
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    public static Dialog createLoadingBar(Context context) {
        Dialog loadingBar = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        loadingBar.setCancelable(false);
        loadingBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        View loadingView = inflater.inflate(R.layout.loading_bar, null);
        loadingBar.setContentView(loadingView);
        loadingBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0effffff")));
        return loadingBar;
    }


    public static String GenRan() {
        String res = "", str = "qwertyuiopasdfghjklzxcvbnm1234567890";
        Random r = new Random();
        int t = r.nextInt(7 - 5) + 5;
        for (int i = 0; i < t; i++) {
            int t2 = r.nextInt(str.length() - 1);
            res += str.charAt(t2);
        }
        return res;
    }


    public static String replaceArabicNumbers(CharSequence original) {
        if (original != null) {
            return original.toString().replaceAll("١", "1")
                    .replaceAll("٢", "2")
                    .replaceAll("٣", "3")
                    .replaceAll("٤", "4")
                    .replaceAll("٥", "5")
                    .replaceAll("٦", "6")
                    .replaceAll("٧", "7")
                    .replaceAll("٨", "8")
                    .replaceAll("٩", "9")
                    .replaceAll("٠", "0");
        }

        return null;
    }


    public static void setGlideImage(Context context, int image, ImageView imageView) {

        Glide.with(context).load(image).into(imageView);
    }

    public static String getDeviceId(Activity context) {
        final TelephonyManager tm = (TelephonyManager) context.getBaseContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = ""
                + android.provider.Settings.Secure.getString(
                context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(),
                ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();

    }

    public static void colorTopAndBottomBars(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = ((Activity) context).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#8b4252"));
            window.setNavigationBarColor(Color.parseColor("#8b4252"));
        }
    }



    public static void changeLanguage(Context context, String languageToLoad) {
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
        SharedPreferenceHelper.setLanguage(context, languageToLoad);
    }

    public static void setSelectedLanguage(Context context) {
        Locale locale = new Locale(SharedPreferenceHelper.getCurrentLanguage(context));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

    }


    public static boolean checkIfAlreadyhavePermission(Context context , String permission) {
        int result = ContextCompat.checkSelfPermission(context, permission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    public static String getCurrentDate(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(c);
    }



}
