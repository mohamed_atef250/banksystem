package ohdone.versionone.utils;

import java.security.PublicKey;

/**
 * Created by sameh on 4/2/18.
 */

public class WebServiceConstants {
    public static final String DEFAULT_LINK = "http://health.my-staff.net/public/api/";
    public static final String PATIENT_LINK=DEFAULT_LINK+"Patient/";
    public static final String COUNTRIES= DEFAULT_LINK+"Countries/";
    public static final String AREAS= DEFAULT_LINK+"Area/";
    public static final String GOVERNORATE= DEFAULT_LINK+"governorate/";
    public static final String SPECIALIZATION_LIST = DEFAULT_LINK+"Specialization/list";
    public static final String SIGN_UP= PATIENT_LINK+"register/";
    public static final String DEVICE_INFO=PATIENT_LINK+"add_device_info";
}
