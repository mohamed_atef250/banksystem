package ohdone.versionone.utils;

import android.content.Context;

/**
 * Created by mohamedatef on 6/15/18.
 */

public class AppSettings {

    public static String CURRENCY_EN = "EGP";
    public static String CURRENCY_AR = "حنيه مصري";


    public static String getCurrency(Context context){
        String language = SharedPreferenceHelper.getCurrentLanguage(context);
        return language.equals("en")?CURRENCY_EN:CURRENCY_AR;
    }


}
