package ohdone.versionone.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LocationResponse{

	@SerializedName("locations")
	private List<LocationsItem> locations;

	@SerializedName("state")
	private int state;

	public void setLocations(List<LocationsItem> locations){
		this.locations = locations;
	}

	public List<LocationsItem> getLocations(){
		return locations;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"LocationResponse{" + 
			"locations = '" + locations + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}