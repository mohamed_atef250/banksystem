package ohdone.versionone.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class BankResponse {

	@SerializedName("state")
	private int state;

	@SerializedName("categories")
	private List<BanksItem> categories;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setCategories(List<BanksItem> categories){
		this.categories = categories;
	}

	public List<BanksItem> getCategories(){
		return categories;
	}

	@Override
 	public String toString(){
		return 
			"BankResponse{" +
			"state = '" + state + '\'' + 
			",categories = '" + categories + '\'' + 
			"}";
		}
}