package ohdone.versionone.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class AdsResponse{

	@SerializedName("ads")
	private List<AdsItem> ads;

	@SerializedName("state")
	private int state;

	public void setAds(List<AdsItem> ads){
		this.ads = ads;
	}

	public List<AdsItem> getAds(){
		return ads;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"AdsResponse{" + 
			"ads = '" + ads + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}