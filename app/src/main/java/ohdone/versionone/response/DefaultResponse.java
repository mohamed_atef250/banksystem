package ohdone.versionone.response;


import com.google.gson.annotations.SerializedName;

public class DefaultResponse{

	@SerializedName("result")
	private Object result;

	@SerializedName("state")
	private int state;

	public void setResult(Object result){
		this.result = result;
	}

	public Object getResult(){
		return result;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"DefaultResponse{" + 
			"result = '" + result + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}