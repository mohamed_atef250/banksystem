package ohdone.versionone.response;

import com.google.gson.annotations.SerializedName;

public class AboutUsItem{

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	@Override
 	public String toString(){
		return 
			"AboutUsItem{" + 
			"details = '" + details + '\'' + 
			",id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}
}