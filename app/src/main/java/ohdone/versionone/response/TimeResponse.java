package ohdone.versionone.response;


import com.google.gson.annotations.SerializedName;


public class TimeResponse{

	@SerializedName("result")
	private Result result;

	@SerializedName("state")
	private int state;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"TimeResponse{" + 
			"result = '" + result + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}