package ohdone.versionone.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class QuestionResponse{

	@SerializedName("questions")
	private List<QuestionsItem> questions;

	@SerializedName("state")
	private int state;

	public void setQuestions(List<QuestionsItem> questions){
		this.questions = questions;
	}

	public List<QuestionsItem> getQuestions(){
		return questions;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"QuestionResponse{" + 
			"questions = '" + questions + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}