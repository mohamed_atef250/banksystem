package ohdone.versionone.response;

import com.google.gson.annotations.SerializedName;

public class LocationsItem{

	@SerializedName("phone")
	private String phone;

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private int id;

	@SerializedName("lang")
	private double lang;

	@SerializedName("title")
	private String title;

	@SerializedName("lat")
	private double lat;

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLang(double lang){
		this.lang = lang;
	}

	public double getLang(){
		return lang;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setLat(double lat){
		this.lat = lat;
	}

	public double getLat(){
		return lat;
	}

	@Override
 	public String toString(){
		return 
			"LocationsItem{" + 
			"phone = '" + phone + '\'' + 
			",details = '" + details + '\'' + 
			",id = '" + id + '\'' + 
			",lang = '" + lang + '\'' + 
			",title = '" + title + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}