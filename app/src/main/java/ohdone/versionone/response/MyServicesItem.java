package ohdone.versionone.response;


import com.google.gson.annotations.SerializedName;


public class MyServicesItem {

	@SerializedName("date")
	private String date;

	@SerializedName("service_type")
	private String serviceType;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("bank_id")
	private String bankId;

	@SerializedName("service_name")
	private String serviceName;

	@SerializedName("user_phone")
	private String userPhone;

	@SerializedName("id")
	private String id;

	@SerializedName("time")
	private String time;

	@SerializedName("customers")
	private String customers;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setServiceType(String serviceType){
		this.serviceType = serviceType;
	}

	public String getServiceType(){
		return serviceType;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setBankId(String bankId){
		this.bankId = bankId;
	}

	public String getBankId(){
		return bankId;
	}

	public void setServiceName(String serviceName){
		this.serviceName = serviceName;
	}

	public String getServiceName(){
		return serviceName;
	}

	public void setUserPhone(String userPhone){
		this.userPhone = userPhone;
	}

	public String getUserPhone(){
		return userPhone;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setCustomers(String customers){
		this.customers = customers;
	}

	public String getCustomers(){
		return customers;
	}

	@Override
 	public String toString(){
		return 
			"MyServicesItem{" +
			"date = '" + date + '\'' + 
			",service_type = '" + serviceType + '\'' + 
			",user_id = '" + userId + '\'' + 
			",bank_id = '" + bankId + '\'' + 
			",service_name = '" + serviceName + '\'' + 
			",user_phone = '" + userPhone + '\'' + 
			",id = '" + id + '\'' + 
			",time = '" + time + '\'' + 
			",customers = '" + customers + '\'' + 
			"}";
		}
}