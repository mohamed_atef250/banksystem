package ohdone.versionone.response;

import com.google.gson.annotations.SerializedName;

public class SocialMediaItem{

	@SerializedName("image")
	private String image;

	@SerializedName("link")
	private String link;

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private int id;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SocialMediaItem{" + 
			"image = '" + image + '\'' + 
			",link = '" + link + '\'' + 
			",details = '" + details + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}