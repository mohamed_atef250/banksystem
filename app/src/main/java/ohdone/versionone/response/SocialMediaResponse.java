package ohdone.versionone.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SocialMediaResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("social_media")
	private List<SocialMediaItem> socialMedia;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setSocialMedia(List<SocialMediaItem> socialMedia){
		this.socialMedia = socialMedia;
	}

	public List<SocialMediaItem> getSocialMedia(){
		return socialMedia;
	}

	@Override
 	public String toString(){
		return 
			"SocialMediaResponse{" + 
			"state = '" + state + '\'' + 
			",social_media = '" + socialMedia + '\'' + 
			"}";
		}
}