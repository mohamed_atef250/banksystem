package ohdone.versionone.response;


import com.google.gson.annotations.SerializedName;


public class Result{

	@SerializedName("number")
	private int number;

	@SerializedName("time")
	private String time;

	public void setNumber(int number){
		this.number = number;
	}

	public int getNumber(){
		return number;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	@Override
 	public String toString(){
		return 
			"Result{" + 
			"number = '" + number + '\'' + 
			",time = '" + time + '\'' + 
			"}";
		}
}