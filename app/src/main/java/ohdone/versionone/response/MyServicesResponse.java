package ohdone.versionone.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class MyServicesResponse{

	@SerializedName("result")
	private List<MyServicesItem> result;

	@SerializedName("state")
	private int state;

	public void setResult(List<MyServicesItem> result){
		this.result = result;
	}

	public List<MyServicesItem> getResult(){
		return result;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"MyServicesResponse{" + 
			"result = '" + result + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}