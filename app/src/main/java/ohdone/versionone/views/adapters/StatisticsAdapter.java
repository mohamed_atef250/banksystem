package ohdone.versionone.views.adapters;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.models.OperationModel;
import ohdone.versionone.views.OnMenuItemClickListner;


public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter

    private ArrayList<OperationModel> menuItems;
    private Context context;
    private OnMenuItemClickListner onMenuItemClickListner;

    // Constructor of the class
    public StatisticsAdapter(Context context, ArrayList<OperationModel> menuItems , OnMenuItemClickListner onMenuItemClickListner) {
        this.context = context;
        this.menuItems = menuItems;
        this.onMenuItemClickListner =onMenuItemClickListner;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return menuItems == null ? 0 : menuItems.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_operation, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final OperationModel menuModel = menuItems.get(position);
        holder.titleTextView.setText(menuModel.getTitle());
        holder.menuImageView.setImageDrawable(context.getResources().getDrawable(menuModel.getIcon()));


        if((position&1)==0){
            holder.itemView.setBackgroundColor(Color.parseColor("#DBE8F1"));
        }else {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          onMenuItemClickListner.setOnMenuItemClickListner(position);
            }
        });
    }

    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_menu_title)
        TextView titleTextView;
        @BindView(R.id.iv_menu_image)
        ImageView menuImageView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}