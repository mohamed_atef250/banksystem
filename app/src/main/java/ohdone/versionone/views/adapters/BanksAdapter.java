package ohdone.versionone.views.adapters;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.BanksItem;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.utils.ViewOperations;
import ohdone.versionone.views.fragments.MyCurrentServicesFragment;
import ohdone.versionone.vollyutils.ConnectionPresenter;


public class BanksAdapter extends RecyclerView.Adapter<BanksAdapter.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter

    private ArrayList<BanksItem> categoriesItems;
    private Context context;
    private int itemWidth;
    private int imageWidth;
    private int dimDP5;

    // Constructor of the class
    public BanksAdapter(Context context, ArrayList<BanksItem> categoriesItems) {
        this.context = context;
        this.categoriesItems = categoriesItems;
        this.itemWidth = ViewOperations.getScreenWidth(context)/2;
        this.imageWidth = ViewOperations.getScreenWidth(context)/3;
        dimDP5=context.getResources().getDimensionPixelOffset(R.dimen.dp5w);
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return categoriesItems == null ? 0 : categoriesItems.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bank, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final BanksItem categoriesItem = categoriesItems.get(position);
        holder.titleTextView.setText(categoriesItem.getTitle());
        holder.categoryContainerLayout.setLayoutParams(new FrameLayout.LayoutParams(itemWidth,itemWidth));
        holder.photoImageView.setLayoutParams(new LinearLayout.LayoutParams(imageWidth-dimDP5,imageWidth-dimDP5));
        ConnectionPresenter.loadImage(holder.photoImageView,"banks/"+categoriesItem.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyCurrentServicesFragment myCurrentServicesFragment =  new MyCurrentServicesFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("bank_id",categoriesItem.getId());
                myCurrentServicesFragment.setArguments(bundle);
                FragmentHelper.addFragment(context,myCurrentServicesFragment,"MyCurrentServicesFragment");
            }
        });
    }

    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_category_title)
        TextView titleTextView;
        @BindView(R.id.iv_category_image)
        ImageView photoImageView;
        @BindView(R.id.ll_category_container)
        LinearLayout categoryContainerLayout;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}