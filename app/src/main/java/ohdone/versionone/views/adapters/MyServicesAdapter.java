package ohdone.versionone.views.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.MyServicesItem;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.views.fragments.ServicesFragment;


public class MyServicesAdapter extends RecyclerView.Adapter<MyServicesAdapter.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter

    private ArrayList<MyServicesItem> myServicesItems;
    private Context context;


    // Constructor of the class
    public MyServicesAdapter(Context context, ArrayList<MyServicesItem> categoriesItems) {
        this.context = context;
        this.myServicesItems = categoriesItems;

    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return myServicesItems == null ? 0 : myServicesItems.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_service, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        MyServicesItem myServicesItem = myServicesItems.get(position);
        holder.titleTextView.setText(myServicesItem.getServiceName());
        holder.numberTextView.setText(""+(myServicesItem.getId()));
        holder.hoursTextView.setText(context.getResources().getString(R.string.label_at)+" : "+myServicesItem.getTime());
        holder.customersTextView.setText(context.getResources().getString(R.string.customers)+ " : "+myServicesItem.getCustomers());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(context,new ServicesFragment(),"ServicesFragment");
            }
        });
    }

    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_service_title)
        TextView titleTextView;
        @BindView(R.id.tv_service_hours)
        TextView hoursTextView;
        @BindView(R.id.tv_service_customers)
        TextView customersTextView;
        @BindView(R.id.iv_my_service_number)
        TextView numberTextView;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}