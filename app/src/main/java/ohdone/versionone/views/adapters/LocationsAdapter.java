package ohdone.versionone.views.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.MapDemoActivity;
import ohdone.versionone.R;
import ohdone.versionone.response.LocationsItem;
import ohdone.versionone.utils.MUT;


public class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.ViewHolder> {
    private ArrayList<LocationsItem> locationsItems;
    private Context context;

    public LocationsAdapter(Context context, ArrayList<LocationsItem> locationsItems) {
        this.context = context;
        this.locationsItems = locationsItems;
    }

    @Override
    public int getItemCount() {
        return locationsItems == null ? 0 : locationsItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        LocationsItem locationsItem = locationsItems.get(position);
        holder.titleTextView.setText(locationsItem.getTitle());


        holder.mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MapDemoActivity.class);
                intent.putExtra("lat", locationsItems.get(position).getLat());
                intent.putExtra("lang", locationsItems.get(position).getLang());
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetails(position);
            }
        });

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_location_title)
        TextView titleTextView;
        @BindView(R.id.btn_location_map)
        Button mapBtn;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    private void showDetails(final int position) {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        dialogBuilder
                .withTitle(locationsItems.get(position).getTitle())
                .withTitleColor("#FFFFFF")
                .withDividerColor(context.getResources().getColor(R.color.colorPrimaryLight))
                .withMessage(locationsItems.get(position).getDetails())
                .withMessageColor("#FFFFFFFF")
                .withDialogColor(context.getResources().getColor(R.color.colorPrimary))
                .withDuration(700)
                .withEffect(Effectstype.Sidefill)
                .withButton1Text(context.getResources().getString(R.string.dialog_action_call))
                .isCancelableOnTouchOutside(true)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (MUT.checkIfAlreadyhavePermission(context, Manifest.permission.CALL_PHONE)) {
                            MUT.makeCall(context, locationsItems.get(position).getPhone());
                            dialogBuilder.dismiss();
                            dialogBuilder.cancel();
                        }else {
                            ActivityCompat.requestPermissions((AppCompatActivity)context, new String[]{Manifest.permission.CALL_PHONE}, 901);
                        }

                    }
                })
                .show();
    }

}