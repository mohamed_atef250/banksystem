package ohdone.versionone.views.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import ohdone.versionone.R;
import ohdone.versionone.response.BanksItem;
import ohdone.versionone.response.TimeResponse;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.utils.MUT;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter

    private ArrayList<BanksItem> categoriesItems;
    private Context context;
    int bankId=0;


    // Constructor of the class
    public ServicesAdapter(Context context, ArrayList<BanksItem> categoriesItems, int bankId) {
        this.context = context;
        this.categoriesItems = categoriesItems;
        this.bankId=bankId;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return categoriesItems == null ? 0 : categoriesItems.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final BanksItem banksItem = categoriesItems.get(position);
        holder.titleTextView.setText(banksItem.getTitle());

        ConnectionPresenter.loadImage(holder.photoImageView,"banks_services/"+banksItem.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SweetAlertDialog sweetAlertDialog =   new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(banksItem.getTitle())
                        .setContentText(context.getResources().getString(R.string.msg_your_selected)+"  ")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                saveMyTime(banksItem.getId());
                                sweetAlertDialog.dismiss();
                            }
                        });
                sweetAlertDialog.show();



            }
        });
    }

    private void saveMyTime(int serviceType){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "save_time");
            jsonObject.put("user_id", SharedPreferenceHelper.getUserDetails(context).getId());
            jsonObject.put("service_type", serviceType);
            jsonObject.put("bank_id", bankId);
            jsonObject.put("date", MUT.getCurrentDate());
            Log.e("KO",jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(context, new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                TimeResponse timeResponse = new Gson().fromJson(response.toString(), TimeResponse.class);
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Booked Successfully!")
                        .setContentText("Your Number is "+(timeResponse.getResult().getNumber()+104)+" At "+timeResponse.getResult().getTime())
                        .show();
                FragmentHelper.popLastFragment(context);
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject, true);
    }
    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_category_title)
        TextView titleTextView;
        @BindView(R.id.iv_category_image)
        ImageView photoImageView;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}