package ohdone.versionone.views.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.SocialMediaItem;
import ohdone.versionone.utils.MUT;
import ohdone.versionone.vollyutils.ConnectionPresenter;


public class SocialMediaAdapter extends RecyclerView.Adapter<SocialMediaAdapter.ViewHolder> {

    private ArrayList<SocialMediaItem> socialMediaItems;
    private Context context;

    public SocialMediaAdapter(Context context, ArrayList<SocialMediaItem> socialMediaItems) {
        this.context = context;
        this.socialMediaItems = socialMediaItems;
    }

    @Override
    public int getItemCount() {
        return socialMediaItems == null ? 0 : socialMediaItems.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_social_media, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
       final SocialMediaItem socialMediaItem = socialMediaItems.get(position);
        ConnectionPresenter.loadImage(holder.socialMediaIcon,"social_media/"+socialMediaItem.getImage());
        holder.nameTextView.setText(socialMediaItem.getDetails());
        holder.linkTextView.setText(socialMediaItem.getLink());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MUT.startWebPage(context,socialMediaItem.getLink());
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_social_media_icon)
        ImageView socialMediaIcon;
        @BindView(R.id.tv_location_name)
        TextView nameTextView;
        @BindView(R.id.tv_social_media_link)
        TextView linkTextView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}