package ohdone.versionone.views.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.QuestionsItem;


public class FaqsAdapter extends RecyclerView.Adapter<FaqsAdapter.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter

    private ArrayList<QuestionsItem> questionsItems;
    private Context context;

    // Constructor of the class
    public FaqsAdapter(Context context, ArrayList<QuestionsItem> questionsItems) {
        this.context = context;
        this.questionsItems = questionsItems;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return questionsItems == null ? 0 : questionsItems.size();
    }


    // specify the row layout file and click for each row
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_faq, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final QuestionsItem questionsItem = questionsItems.get(position);
        holder.questionTextView.setText(questionsItem.getQuestion());

        holder.answerTextView.setText(questionsItem.getAnswer());

        holder.faqContainerLayout.setBackgroundColor(context.getResources().getColor(((position & 1) == 0) ? R.color.colorLightGray : R.color.colorWhite));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    // Static inner class to initialize the views of rows
    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_faq_question)
        TextView questionTextView;

        @BindView(R.id.tv_faq_answer)
        TextView answerTextView;


        @BindView(R.id.ll_faq_container)
        LinearLayout faqContainerLayout;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}