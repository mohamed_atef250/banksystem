package ohdone.versionone.views.activties;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.UserResponse;
import ohdone.versionone.utils.Check;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.views.activties.MainActivity;
import ohdone.versionone.views.activties.SplashScreenActivity;
import ohdone.versionone.views.fragments.HomeFragment;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class LoginActivity extends AppCompatActivity {
    View rootView;

    @BindView(R.id.et_login_email_or_phone)
    EditText emailPhoneEditText;
    @BindView(R.id.et_login_password)
    EditText passwordEditText;

    @BindView(R.id.btn_login_enter)
    Button registerBtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);
        ButterKnife.bind(this);
        setEvents();
    }



    private Context getActivity(){
        return  this;
    }

    private void login(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method","login");
            jsonObject.put("email",emailPhoneEditText.getText()+"");
            jsonObject.put("phone",emailPhoneEditText.getText()+"");
            jsonObject.put("password",passwordEditText.getText()+"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
               UserResponse userResponse =  new Gson().fromJson(response.toString(), UserResponse.class);
                 userResponse.getUser().get(0).setPassword(passwordEditText.getText()+"");

                if(userResponse.getState()==ConnectionPresenter.SUCCESS){
                    userResponse.getUser().get(0).setPassword(passwordEditText.getText()+"");
                    SharedPreferenceHelper.saveUserDetails(getActivity(),userResponse.getUser().get(0));
                    SharedPreferenceHelper.saveUserDetails(getActivity(),userResponse.getUser().get(0));
                    finishAffinity();
                    startActivity(new Intent(getActivity(),SplashScreenActivity.class));
                    // Toast.makeText(getActivity(), userResponse.getUser().get(0).getName()+" "+userResponse.getUser().get(0).getId(), Toast.LENGTH_SHORT).show();
                }else if(userResponse.getState()==ConnectionPresenter.WRONG_PASSWORD){
                    Toast.makeText(getActivity(),getResources().getString(R.string.msg_used_email),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject,true);
    }
    private void setEvents(){
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    login();
                }
            }
        });

    }

    private boolean validate(){

        if(Check.isEmpty(passwordEditText.getText()+"")||Check.isEmpty(emailPhoneEditText.getText()+"")){
            Toast.makeText(getActivity(), ""+getResources().getString(R.string.msg_fill_data), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}