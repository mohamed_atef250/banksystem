package ohdone.versionone.views.activties;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ss.bottomnavigation.BottomNavigation;
import com.ss.bottomnavigation.events.OnSelectedItemChangeListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.utils.MUT;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.views.fragments.AboutUsFragment;
import ohdone.versionone.views.fragments.BanksFragment;
import ohdone.versionone.views.fragments.LocationsFragment;
import ohdone.versionone.views.fragments.MyAccountFragment;
import ohdone.versionone.views.fragments.QuestionsFragment;


public class MainActivity extends AppCompatActivity {


    @BindView(R.id.iv_main_profile)
    ImageView favouriteImageView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
        if (MUT.checkIfAlreadyhavePermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && MUT.checkIfAlreadyhavePermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            setupHome();
        } else {
            requestPermissions();
        }
    }

    private void setupMenu() {

        BottomNavigation bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setMode(BottomNavigation.MODE_PHONE);
        bottomNavigation.setEnabled(true);
        bottomNavigation.setOnSelectedItemChangeListener(new OnSelectedItemChangeListener() {
            @Override
            public void onSelectedItemChanged(int itemId) {
                Fragment fragment = null;
                switch (itemId) {
                    case R.id.tab_home:
                        fragment = new BanksFragment();
                        break;
                    //case R.id.tab_images:
                      //  fragment = new SocialMediaFragment();
                      //  break;
                    case R.id.tab_camera:
                        fragment = new LocationsFragment();
                        break;
                    case R.id.tab_products:
                        fragment = new QuestionsFragment();
                        break;
                    case R.id.tab_more:
                        fragment = new AboutUsFragment();
                        break;
                }

                if (fragment != null) {
                    FragmentHelper.popAllFragments(MainActivity.this);
                    FragmentHelper.replaceFragment(MainActivity.this, fragment, "");

                }
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupHome();
                } else {
                    requestPermissions();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

        private void requestPermissions() {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        }

    private void setupHome() {
        FragmentHelper.addFragment(this, new BanksFragment(), "");
        setupMenu();
        setEvents();
    }

    private void setEvents() {


        favouriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SharedPreferenceHelper.isLogined(MainActivity.this,false)) {
                    FragmentHelper.replaceFragment(MainActivity.this, new MyAccountFragment(), "");
                }else {
                    //FragmentHelper.replaceFragment(MainActivity.this, new LoginActivity(), "");
                }
            }
        });




    }


}
