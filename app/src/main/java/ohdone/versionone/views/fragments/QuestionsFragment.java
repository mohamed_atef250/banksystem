package ohdone.versionone.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.QuestionResponse;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.utils.ViewOperations;
import ohdone.versionone.views.adapters.FaqsAdapter;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class QuestionsFragment extends Fragment {
    View rootView;
    @BindView(R.id.rv_faq_collection)
    RecyclerView questionsRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_faq, container, false);
        ButterKnife.bind(this,rootView);
        getQuestions();
        return rootView;
    }

    private void getQuestions(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("method","questions");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                QuestionResponse questionResponse =  new Gson().fromJson(response.toString(), QuestionResponse.class);

                if(questionResponse.getState()==ConnectionPresenter.SUCCESS){
                    FaqsAdapter faqsAdapter = new FaqsAdapter(getActivity(),new ArrayList<>(questionResponse.getQuestions()));
                    questionsRecyclerView.setAdapter(faqsAdapter);
                    ViewOperations.setRVHVertical(getActivity(),questionsRecyclerView);
                }
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject,true);
    }

}