package ohdone.versionone.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.DefaultResponse;
import ohdone.versionone.response.SocialMediaResponse;
import ohdone.versionone.utils.Check;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.utils.ViewOperations;
import ohdone.versionone.views.adapters.SocialMediaAdapter;

import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class SocialMediaFragment extends Fragment {
    View rootView;

    @BindView(R.id.rv_social_media_collection)
    RecyclerView socialMediaRecycleView;

    @BindView(R.id.et_social_media_title)
    EditText titleEditText;
    @BindView(R.id.et_social_media_details)
    EditText detailsEditText;

    @BindView(R.id.btn_social_media_send)
    Button sendBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_social_media, container, false);
        ButterKnife.bind(this,rootView);
        getSocialMedia();
        setEvents();
        return rootView;
    }

    private void getSocialMedia(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("method","social_media");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                SocialMediaResponse socialMediaResponse =  new Gson().fromJson(response.toString(), SocialMediaResponse.class);

                if(socialMediaResponse.getState()==ConnectionPresenter.SUCCESS){
                    SocialMediaAdapter socialMediaAdapter = new SocialMediaAdapter(getActivity(),new ArrayList<>(socialMediaResponse.getSocialMedia()));
                    socialMediaRecycleView.setAdapter(socialMediaAdapter);
                    ViewOperations.setRVHVertical(getActivity(),socialMediaRecycleView);
                }
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject,true);
    }


    private void sendMessage(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method","send_message");
            jsonObject.put("phone_or_mail",titleEditText.getText());
            jsonObject.put("message",detailsEditText.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                DefaultResponse defaultResponse =  new Gson().fromJson(response.toString(), DefaultResponse.class);

                if(defaultResponse.getState()==ConnectionPresenter.SUCCESS){
                    Toast.makeText(getActivity(), ""+getResources().getString(R.string.message_sent), Toast.LENGTH_SHORT).show();
                    titleEditText.setText("");
                    detailsEditText.setText("");
                }
            }

            @Override
            public void onRequestError(Object error) {
            }
        });

        connectionPresenter.connect(jsonObject,true);
    }
    private void setEvents(){
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    sendMessage();
                }
            }
        });
    }

    private boolean validate(){
        if(Check.isEmpty(titleEditText.getText()+"")||Check.isEmpty(detailsEditText.getText()+"")){
            Toast.makeText(getActivity(), ""+getResources().getString(R.string.msg_fill_data), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}