package ohdone.versionone.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import ohdone.versionone.R;
import ohdone.versionone.response.AdsItem;
import ohdone.versionone.response.AdsResponse;
import ohdone.versionone.response.TimeResponse;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.utils.MUT;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class HomeFragment extends Fragment {
    View rootView;
    @BindView(R.id.btn_home_book_now)
    Button bookNowBtn;

    @BindView(R.id.btn_home_products)
    Button productsBtn;

    @BindView(R.id.btn_home_top_new)
    Button topNewsBtn;

    @BindView(R.id.btn_home_call_us)
    Button callUsBtn;

    @BindView(R.id.sl_home_ads)
    SliderLayout adsSliderLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        getAds();
        setEvents();
        return rootView;
    }

    private void setEvents() {
        productsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SharedPreferenceHelper.isLogined(getActivity(),true)) {
                    FragmentHelper.addFragment(getActivity(), new MyAccountFragment(), "ProductsFragment2");
                }
            }
        });

        topNewsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentHelper.addFragment(getActivity(), new LocationsFragment(), "ProductsFragment5");
            }
        });

        callUsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentHelper.addFragment(getActivity(), new SocialMediaFragment(), "SocialMediaFragment");
            }
        });

        bookNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectService();
            }
        });
    }

    private void getAds() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("method", "ads");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                AdsResponse adsResponse = new Gson().fromJson(response.toString(), AdsResponse.class);
                List<AdsItem> adsItems = adsResponse.getAds();

                for (int i = 0; i < adsItems.size(); i++) {
                    TextSliderView textSliderView = new TextSliderView(getActivity());
                    // initialize a SliderLayout
                    Log.e("Link", ConnectionPresenter.filesLink + "ads/" + adsItems.get(i).getImage());
                    textSliderView.description(adsItems.get(i).getDetails())
                            .image(ConnectionPresenter.filesLink + "ads/" + adsItems.get(i).getImage())
                            .setScaleType(BaseSliderView.ScaleType.CenterCrop);

                    adsSliderLayout.addSlider(textSliderView);
                }

                adsSliderLayout.setPresetTransformer(SliderLayout.Transformer.RotateDown);
                adsSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                adsSliderLayout.setCustomAnimation(new DescriptionAnimation());
                adsSliderLayout.setDuration(4000);

            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject, true);
    }

    int selectedItem = 0;

    private void selectService() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setIcon(R.mipmap.ic_launcher);
        builderSingle.setTitle(getResources().getString(R.string.msg_select_service));

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Customer service");
        arrayAdapter.add("Add money");
        arrayAdapter.add("Get money");
        arrayAdapter.add("Create credit card");
        arrayAdapter.add("Buy a Car");

        builderSingle.setNegativeButton(getActivity().getResources().getString(R.string.dialog_action_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedItem = which;
                String strName = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(getActivity());
                builderInner.setMessage(strName);
                builderInner.setTitle(getActivity().getResources().getString(R.string.msg_your_selected));
                builderInner.setPositiveButton(getActivity().getResources().getString(R.string.dialog_action_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setMyPhone(which);
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        builderSingle.show();
    }

    private void setMyPhone(final int service){
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        final EditText edittext = new EditText(getActivity());
        alert.setMessage(getActivity().getResources().getString(R.string.dialog_msg_phone));
        alert.setTitle(getActivity().getResources().getString(R.string.dialog_msg_add_phone));

        alert.setView(edittext);

        alert.setPositiveButton(getActivity().getResources().getString(R.string.dialog_action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String phone = edittext.getText().toString();
                saveMyTime(phone,service);
            }
        });

        alert.setNegativeButton(getActivity().getResources().getString(R.string.dialog_action_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }



    private void saveMyTime(String phone,int serviceType){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("method", "save_time");
            jsonObject.put("user_phone", phone);
            jsonObject.put("service_type", serviceType);
            jsonObject.put("date", MUT.getCurrentDate());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                TimeResponse timeResponse = new Gson().fromJson(response.toString(), TimeResponse.class);
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Booked Successfully!")
                        .setContentText("Your Number is "+timeResponse.getResult().getNumber()+" At "+timeResponse.getResult().getTime())
                        .show();
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject, true);
    }
}