package ohdone.versionone.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.UserResponse;
import ohdone.versionone.utils.Check;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class RegisterFragment extends Fragment {
    View rootView;

    @BindView(R.id.et_register_name)
    EditText nameEditText;
    @BindView(R.id.et_register_email)
    EditText emailEditText;
    @BindView(R.id.et_register_phone)
    EditText phoneEditText;
    @BindView(R.id.et_register_address)
    EditText addressEditText;
    @BindView(R.id.et_register_password)
    EditText passwordEditText;
    @BindView(R.id.et_register_confirm_password)
    EditText confirmPasswordEditText;
    @BindView(R.id.iv_register_image)
    ImageView photoImageView;

    @BindView(R.id.btn_register_enter)
    Button registerBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this,rootView);
        setEvents();
        return rootView;
    }



    private void register(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method","register");
            jsonObject.put("name",nameEditText.getText()+"");
            jsonObject.put("email",emailEditText.getText()+"");
            jsonObject.put("phone",phoneEditText.getText()+"");
            jsonObject.put("address",addressEditText.getText()+"");
            jsonObject.put("password",passwordEditText.getText()+"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
               UserResponse userResponse =  new Gson().fromJson(response.toString(), UserResponse.class);

                if(userResponse.getState()==ConnectionPresenter.SUCCESS){
                    userResponse.getUser().get(0).setPassword(passwordEditText.getText()+"");
                    SharedPreferenceHelper.saveUserDetails(getActivity(),userResponse.getUser().get(0));
                    Toast.makeText(getActivity(), ""+getResources().getString(R.string.msg_registered), Toast.LENGTH_SHORT).show();
                }else if(userResponse.getState()==ConnectionPresenter.USED_MAIL){
                    Toast.makeText(getActivity(),getResources().getString(R.string.msg_used_email),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject,true);
    }
    private void setEvents(){
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    register();
                }
            }
        });

        photoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private boolean validate(){

        if(Check.isEmpty(nameEditText.getText()+"")||Check.isEmpty(emailEditText.getText()+"")||Check.isEmpty(phoneEditText.getText()+"")||Check.isEmpty(addressEditText.getText()+"")||Check.isEmpty(confirmPasswordEditText.getText()+"")){
            Toast.makeText(getActivity(), ""+getResources().getString(R.string.msg_fill_data), Toast.LENGTH_SHORT).show();
            return false;
        }else if(!Check.isEqual(passwordEditText.getText()+"",confirmPasswordEditText.getText()+"")){
            Toast.makeText(getActivity(), ""+getResources().getString(R.string.msg_passwords_not_matches), Toast.LENGTH_SHORT).show();
            return false;
        }else if(!Check.isMail(emailEditText.getText()+"")){
            Toast.makeText(getActivity(), ""+getResources().getString(R.string.msg_enter_valid_mail), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}