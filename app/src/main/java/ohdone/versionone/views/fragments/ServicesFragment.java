package ohdone.versionone.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.BankResponse;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.utils.ViewOperations;
import ohdone.versionone.views.adapters.ServicesAdapter;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class ServicesFragment extends Fragment {
    View rootView;
    @BindView(R.id.rv_categories_container)
    RecyclerView categoriesRecyclerView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_services, container, false);
        ButterKnife.bind(this,rootView);
        getCategories();
        return rootView;
    }

    private void getCategories(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("method","banks_services");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                BankResponse bankResponse =  new Gson().fromJson(response.toString(), BankResponse.class);

                if(bankResponse.getState()== ConnectionPresenter.SUCCESS){
                    ServicesAdapter servicesAdapter = new ServicesAdapter(getActivity(),new ArrayList<>(bankResponse.getCategories()), getArguments().getInt("bank_id",-1));
                    categoriesRecyclerView.setAdapter(servicesAdapter);
                    ViewOperations.setRVHVertical(getActivity(),categoriesRecyclerView);
                    // ViewOperations.setRVHVerticalGrid(getActivity(),categoriesRecyclerView,2);

                }
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject,true);
    }
}