package ohdone.versionone.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.AllAtMapActivity;
import ohdone.versionone.R;
import ohdone.versionone.response.LocationResponse;
import ohdone.versionone.response.LocationsItem;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.utils.ViewOperations;
import ohdone.versionone.views.adapters.LocationsAdapter;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class LocationsFragment extends Fragment {
    View rootView;
    @BindView(R.id.rv_location_collection)
    RecyclerView locationRecyclerView;
    @BindView(R.id.btn_location_map)
    Button showOnMapBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_location, container, false);
        ButterKnife.bind(this, rootView);
        getLocations();
        showOnMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(locationsItems!=null){
                    Intent intent = new Intent(getActivity(), AllAtMapActivity.class);
                    Gson gson = new Gson();
                    String json = gson.toJson(locationResponse);
                    intent.putExtra("locations", json);
                    startActivity(intent);
                }
            }
        });
        return rootView;
    }
    ArrayList<LocationsItem> locationsItems;
    LocationResponse locationResponse;
    private void getLocations() {

        final String []banks={"Al-Ahly Bank ATM (0.1 KM)","QNB Al Ahli (0.5 KM)","BLOM BANK ATM (1.0 KM)","Al-Ahly Bank ATM (3.5 KM)","CIB Bank ATM(8.5 KM)"};
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("method", "locations");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                 locationResponse = new Gson().fromJson(response.toString(), LocationResponse.class);
                if (locationResponse.getState() == ConnectionPresenter.SUCCESS) {
                      locationsItems = new ArrayList<>(locationResponse.getLocations());

                    for(int i=0; i<locationsItems.size(); i++){
                        locationsItems.get(i).setTitle(banks[i%banks.length]);
                    }
                    LocationsAdapter locationsAdapter = new LocationsAdapter(getActivity(),locationsItems);
                    locationRecyclerView.setAdapter(locationsAdapter);
                    ViewOperations.setRVHVertical(getActivity(), locationRecyclerView);
                }
            }

            @Override
            public void onRequestError(Object error) {
            }
        });

        connectionPresenter.connect(jsonObject, true);
    }
}