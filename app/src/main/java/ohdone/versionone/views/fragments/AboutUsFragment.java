package ohdone.versionone.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.AboutUsItem;
import ohdone.versionone.response.AboutUsResponse;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class AboutUsFragment extends Fragment {
    View rootView;
    @BindView(R.id.iv_about_us_image)
    ImageView photoImageView;
    @BindView(R.id.tv_about_us_title)
    TextView titleTextView;
    @BindView(R.id.tv_about_us_details)
    TextView detailsTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about_us, container, false);
        ButterKnife.bind(this,rootView);
        getAboutUs();
        return rootView;
    }

    private void getAboutUs(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("method","about_us");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                AboutUsResponse aboutUsResponse =  new Gson().fromJson(response.toString(), AboutUsResponse.class);
                AboutUsItem aboutUsItem = aboutUsResponse.getAboutUs().get(0);
                titleTextView.setText(aboutUsItem.getTitle());
                detailsTextView.setText(aboutUsItem.getDetails());
            }

            @Override
            public void onRequestError(Object error) {

            }
        });

        connectionPresenter.connect(jsonObject,true);
    }
}