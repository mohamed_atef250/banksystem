package ohdone.versionone.views.fragments;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;

import ohdone.versionone.models.OperationModel;
import ohdone.versionone.response.DefaultResponse;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.utils.ViewOperations;
import ohdone.versionone.views.OnMenuItemClickListner;
import ohdone.versionone.views.adapters.StatisticsAdapter;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

public class MyAccountFragment extends Fragment {

    @BindView(R.id.pc_my_account_chart)
    public PieChart chart;

    @BindView(R.id.rv_my_account_operations)
    RecyclerView operationsRecyclerView;

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_account, container, false);
        ButterKnife.bind(this, rootView);
        setProfileStatistics();
        setOperatins();
        return rootView;
    }


    private void getProfile() {

    }

    private void setProfileStatistics() {

        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        entries.add(new PieEntry(70f));
        entries.add(new PieEntry(30f));
        PieDataSet dataSet = new PieDataSet(entries, "Operations Statistics");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);


        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.colorPrimaryLight));
        colors.add(getResources().getColor(R.color.colorPrimaryDark));

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();

    }


    private void setOperatins() {
        ArrayList<OperationModel> operationModels = new ArrayList<>();

        for (int i = 0; i < 100; i++) {

            if (i % 3 == 0) {
                operationModels.add(new OperationModel(i, R.drawable.ic_deposite,
                        (100+i)+" EGP Added at " + ((5 + i) % 30) + "-" + ((5 + i) % 12) + "-2019", true));
            } else {
                operationModels.add(new OperationModel(i, R.drawable.ic_withdraw,
                        (150+i)+"100 EGP Removed at " + ((7 + i) % 30) + "-" + ((8 + i) % 12) + "-2019", false));
            }

        }
        StatisticsAdapter statisticsAdapter = new StatisticsAdapter(getActivity(), operationModels, new OnMenuItemClickListner() {
            @Override
            public void setOnMenuItemClickListner(int position) {

            }
        });
        ViewOperations.setRVHVertical(getActivity(), operationsRecyclerView);
        operationsRecyclerView.setAdapter(statisticsAdapter);
    }

}
