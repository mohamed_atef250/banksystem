package ohdone.versionone.views.fragments;



/*public class UpdateFragment extends Fragment {
    View rootView;

    @BindView(R.id.tv_register_cat_msg)
    TextView catMsgTextView;
    @BindView(R.id.et_register_name)
    EditText nameEditText;
    @BindView(R.id.et_register_email)
    EditText emailEditText;
    @BindView(R.id.et_register_phone)
    EditText phoneEditText;
    @BindView(R.id.et_register_address)
    EditText addressEditText;
    @BindView(R.id.et_register_password)
    EditText passwordEditText;
    @BindView(R.id.et_register_confirm_password)
    EditText confirmPasswordEditText;
    @BindView(R.id.iv_register_image)
    ImageView photoImageView;

    @BindView(R.id.et_register_photo)
    EditText storePhotoEditText;

    @BindView(R.id.btn_register_enter)
    Button registerBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, rootView);
        catMsgTextView.setVisibility(View.GONE);
        setEvents();
        setUserDetails();
        registerBtn.setText(getResources().getString(R.string.action_update));
        ConnectionPresenter.loadImage(photoImageView, "stores/" + SharedPreferenceHelper.getUserDetails(getActivity()).getImage());
        return rootView;
    }


    private void update() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "update");
            jsonObject.put("name", nameEditText.getText() + "");
            jsonObject.put("user_id", SharedPreferenceHelper.getUserDetails(getActivity()).getId() + "");
            jsonObject.put("phone", phoneEditText.getText() + "");
            jsonObject.put("address", addressEditText.getText() + "");
            jsonObject.put("password", passwordEditText.getText() + "");
            jsonObject.put("email", emailEditText.getText() + "");
            jsonObject.put("image",  Base64.encodeToString(bytes, 0)+ "");
            jsonObject.put("image_name" , file + "");

        } catch (Exception e) {
            e.printStackTrace();
        }
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                UserResponse userResponse = new Gson().fromJson(response.toString(), UserResponse.class);

                if (userResponse.getState() == ConnectionPresenter.SUCCESS) {
                    userResponse.getUser().get(0).setPassword(passwordEditText.getText() + "");
                    SharedPreferenceHelper.saveUserDetails(getActivity(), userResponse.getUser().get(0));
                    Toast.makeText(getActivity(), "" + getResources().getString(R.string.msg_updated_successfully), Toast.LENGTH_SHORT).show();
                    getActivity().finishAffinity();
                    MUT.startActivity(getActivity(), SplashScreenActivity.class);
                } else if (userResponse.getState() == ConnectionPresenter.USED_MAIL) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.msg_used_by_other), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject, true);
    }

    private void setEvents() {
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    update();
                }
            }
        });

        photoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        storePhotoEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }


    public void selectImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.select_image));
        builder.setItems(new CharSequence[]{getResources().getString(R.string.gallery),
                        getResources().getString(R.string.camera)},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(galleryIntent, 1024);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, 1023);
                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();
    }

    CompressObject compressObject;
    String file;
    byte[] bytes;
    FileOperations fileOperations;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            Uri lastData = data.getData();

            if (lastData == null || data == null) {
                lastData = FileOperations.specialCameraSelector(getActivity(), (Bitmap) data.getExtras().get("data"));
            }

            if (lastData == null) {
                return;
            } else {
                if (fileOperations == null) {
                    fileOperations = new FileOperations();
                }
                file = fileOperations.getPath(getActivity(), lastData);
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_attach_image);
                try {
                    dialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(Color.TRANSPARENT));
                } catch (Exception e) {
                    e.getStackTrace();
                }
                final ImageView attachImage = dialog.findViewById(R.id.iv_attach_image_photo);
                Button send = dialog.findViewById(R.id.btn_attach_image_confirm);
                Button cancel = dialog.findViewById(R.id.btn_attach_image_cancel);
                final File f = new File(file);
                if (f.exists()) {
                    compressObject = new ImageCompression(getActivity()).compressImage(file, f.getName());
                    attachImage.setImageBitmap(compressObject.getImage());
                    bytes = compressObject.getByteStream().toByteArray();
                }

                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        file = f.getName();
                        storePhotoEditText.setText(f.getName());
                        dialog.cancel();
                        compressObject = null;
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setUserDetails() {
        UserItem userItem = SharedPreferenceHelper.getUserDetails(getActivity());
        nameEditText.setText(userItem.getName());
        emailEditText.setText(userItem.getEmail());
        phoneEditText.setText(userItem.getPhone());
        addressEditText.setText(userItem.getAddress());
        passwordEditText.setText(userItem.getPassword());


    }

    private boolean validate() {

        if (Check.isEmpty(nameEditText.getText() + "") || Check.isEmpty(emailEditText.getText() + "") || Check.isEmpty(phoneEditText.getText() + "") || Check.isEmpty(addressEditText.getText() + "") || Check.isEmpty(confirmPasswordEditText.getText() + "")) {
            Toast.makeText(getActivity(), "" + getResources().getString(R.string.msg_fill_data), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Check.isEqual(passwordEditText.getText() + "", confirmPasswordEditText.getText() + "")) {
            Toast.makeText(getActivity(), "" + getResources().getString(R.string.msg_passwords_not_matches), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Check.isMail(emailEditText.getText() + "")) {
            Toast.makeText(getActivity(), "" + getResources().getString(R.string.msg_enter_valid_mail), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}*/