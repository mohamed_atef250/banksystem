package ohdone.versionone.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import butterknife.BindView;
import butterknife.ButterKnife;
import ohdone.versionone.R;
import ohdone.versionone.response.BankResponse;
import ohdone.versionone.response.MyServicesResponse;
import ohdone.versionone.utils.FragmentHelper;
import ohdone.versionone.utils.SharedPreferenceHelper;
import ohdone.versionone.utils.ViewOperations;
import ohdone.versionone.views.adapters.MyServicesAdapter;
import ohdone.versionone.views.adapters.ServicesAdapter;
import ohdone.versionone.vollyutils.ConnectionPresenter;
import ohdone.versionone.vollyutils.ConnectionView;

/**
 * Created by mohamedatef on 6/1/18.
 */

public class MyCurrentServicesFragment extends Fragment {
    View rootView;
    @BindView(R.id.rv_categories_container)
    RecyclerView categoriesRecyclerView;
    @BindView(R.id.btn_my_services_add)
    Button addServiceBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_current_services, container, false);
        ButterKnife.bind(this,rootView);
        getMyCurrentServices();
        setEvents();
        return rootView;
    }


    private void setEvents(){
        addServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicesFragment servicesFragment = new ServicesFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("bank_id", getArguments().getInt("bank_id",-1));
                servicesFragment.setArguments(bundle);
                FragmentHelper.replaceFragment(getActivity(),servicesFragment,"ServicesFragment");
            }
        });
    }
    private String getCurrentDate(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(c);
    }

    private void getMyCurrentServices(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("language", SharedPreferenceHelper.getCurrentLanguage(getActivity()));
            jsonObject.put("method","my_booked_services");
            jsonObject.put("date", getCurrentDate());
            jsonObject.put("user_id",SharedPreferenceHelper.getUserDetails(getActivity()).getId());
            jsonObject.put("bank_id", getArguments().getInt("bank_id",-1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Hi",jsonObject+"") ;
        ConnectionPresenter connectionPresenter = new ConnectionPresenter(getActivity(), new ConnectionView() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                MyServicesResponse myServicesResponse =  new Gson().fromJson(response.toString(), MyServicesResponse.class);
                Log.e("Hi",response+"") ;
                if(myServicesResponse.getState()== ConnectionPresenter.SUCCESS){
                    MyServicesAdapter servicesAdapter = new MyServicesAdapter(getActivity(),new ArrayList<>(myServicesResponse.getResult()));
                    categoriesRecyclerView.setAdapter(servicesAdapter);
                    ViewOperations.setRVHVertical(getActivity(),categoriesRecyclerView);
                }
            }

            @Override
            public void onRequestError(Object error) {
                //Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        });

        connectionPresenter.connect(jsonObject,true);
    }
}