package ohdone.versionone.vollyutils;

import android.app.Dialog;
import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import ohdone.versionone.R;
import ohdone.versionone.utils.MUT;


//here you can find functions that relate to web_service or make connections in other way here you find functions that send and recive data from  and to server


public class ConnectionPresenter {
    public static String baseLink = "https://bmobileapp.000webhostapp.com/";
    public static String defaultLink = baseLink+"web_service/go.php",
            filesLink = baseLink+"files/";

    public static final int SUCCESS = 101,
            EMPTY = 102, USED_MAIL = 105, WRONG_PASSWORD = 106;

    public static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.color.colorTransparent)
            .showImageOnLoading(R.color.colorTransparent)
            .showImageOnFail(R.color.colorTransparent)
            .cacheInMemory(true)
            .cacheOnDisk(true).build();
    public static ImageLoader imageLoader = ImageLoader.getInstance();
    private ConnectionView connectionView;
    private RequestQueue queue;
    private Context context;
    private Dialog loadingBar = null;

    public ConnectionPresenter(Context context, ConnectionView parentView) {
        this.connectionView = parentView;
        this.context = context;
    }


    public void connect(JSONObject jsonObject, final Boolean showLoading) {


        try {
            if (showLoading) {
                loadingBar = MUT.createLoadingBar(context);
                loadingBar.show();
            }
        } catch (Exception e) {

            e.getStackTrace();
        }

        queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, defaultLink, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        if (loadingBar != null && showLoading) {
                            loadingBar.cancel();
                        }
                        try {
                            if (response == null || response.toString().equals("")) {
                                connectionView.onRequestError(null);
                            } else {
                                connectionView.onRequestSuccess(response);
                            }
                        } catch (Exception e) {
                          //  Toast.makeText(context, "" + e.getMessage() + " " + e.getStackTrace(), Toast.LENGTH_SHORT).show();
                            connectionView.onRequestError(null);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (volleyError instanceof ServerError) {
                    MUT.lToast(context, context.getResources().getString(R.string.msg_server_error));
                } else if (volleyError instanceof ParseError) {
                    MUT.lToast(context, context.getResources().getString(R.string.msg_server_error));
                } else {
                  //  MUT.lToast(context, volleyError.getMessage()+" "+volleyError.getStackTrace());
                }

                if (loadingBar != null && showLoading) {
                    loadingBar.cancel();
                }
                connectionView.onRequestError(volleyError);


            }


        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Accept", "application/json");
                return params;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);

    }


    public static void loadImage(final ImageView image, String path) {
        //System.out.println("Image Link : "+filesLink+path);
        imageLoader.displayImage(filesLink + path, image, options);
    }

}